# README #

### Conference management system ###

* Create and cancel conferences
* Check conference room availability
* Add remove participants from conference

### Used technologies ###

* [Java 11](https://jdk.java.net/java-se-ri/11)
* [Spring Boot 2](https://spring.io/projects/spring-boot)
* [Lombok](https://projectlombok.org)
* [Dozer](http://dozer.sourceforge.net)
* [Querydsl](http://www.querydsl.com)
* [DbUnit](http://dbunit.sourceforge.net)
* [REST-assured](https://github.com/rest-assured/rest-assured)

### Install ###
```.bash
mvn clean install
```

### Run ###
```.bash
mvn clean spring-boot:run
```

### Usage ###

API End points listed via swagger
```.url
http://localhost:8098/swagger-ui.html#/
```
