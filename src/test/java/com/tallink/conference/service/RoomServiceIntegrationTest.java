package com.tallink.conference.service;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.tallink.conference.BaseIntegrationTest;
import com.tallink.conference.DBIntegrationTest;
import com.tallink.conference.exception.ResourceNotFoundException;
import com.tallink.conference.exception.RoomAlreadyExistsException;
import com.tallink.conference.model.Room;
import com.tallink.conference.model.dto.CreateRoomDto;
import com.tallink.conference.repository.room.RoomRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

import static com.tallink.conference.DataSetPaths.ROOMS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@DBIntegrationTest
@DatabaseSetup(ROOMS)
public class RoomServiceIntegrationTest extends BaseIntegrationTest {

    @Autowired
    private RoomService roomService;

    @Autowired
    private RoomRepository roomRepository;

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void shouldCreateRoom() {
        var dto = getCreateRoomDto();
        roomService.createRoom(dto);
        var room = roomRepository.findByName(dto.getName())
                .orElseThrow(() -> new ResourceNotFoundException(1L, Room.class));
        assertNotNull(room);
        assertNotNull(room.getId());
        assertEquals(room.getName(), dto.getName());
        assertEquals(room.getLocation(), dto.getLocation());
        assertEquals(room.getMaxSeats(), dto.getMaxSeats());
    }

    public void shouldThrowsRoomAlreadyExistsException() {
        exceptionRule.expect(RoomAlreadyExistsException.class);
        var dto = getCreateRoomDto();
        dto.setName("Name");
        roomService.createRoom(dto);
    }

    @Test
    public void shouldFindAllRooms() {
        var page = PageRequest.of(0, 10);
        var rooms = roomService.findAllRooms(page);
        assertThat(rooms.getContent().size(), is(5));
    }

    @Test
    public void shouldFindAvaliableRoomByConferenceId() {
        var availableRooms = roomService.findAvailableRoomByConferenceId(2L);
        assertThat(availableRooms, notNullValue());
        assertEquals(availableRooms.size(), 5);
    }

    private CreateRoomDto getCreateRoomDto() {
        return CreateRoomDto.builder()
                .name("TestName")
                .location("TestLocation")
                .maxSeats(200)
                .build();
    }
}
