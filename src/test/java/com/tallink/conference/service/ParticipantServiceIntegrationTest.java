package com.tallink.conference.service;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.tallink.conference.BaseIntegrationTest;
import com.tallink.conference.DBIntegrationTest;
import com.tallink.conference.exception.ResourceNotFoundException;
import com.tallink.conference.model.Participant;
import com.tallink.conference.model.dto.CreateParticipantDto;
import com.tallink.conference.repository.ParticipantRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDate;

import static com.tallink.conference.DataSetPaths.PARTICIPANTS;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@DBIntegrationTest
@DatabaseSetup(PARTICIPANTS)
public class ParticipantServiceIntegrationTest extends BaseIntegrationTest {

    @Autowired
    private ParticipantService participantService;
    @Autowired
    private ParticipantRepository participantRepository;

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void shouldFindParticipantById() {
        Long participantId = 1L;
        var participant = participantService.findById(participantId);
        assertThat(participant, notNullValue());
        assertEquals(participant.getId(), participantId);
        assertEquals(participant.getFullName(), "Full Name");
        assertEquals(participant.getBirthDate(), LocalDate.parse("2000-02-02"));
    }

    @Test
    public void shouldThrowsResourceNotFoundExceptionWhenFindParticipantById() {
        exceptionRule.expect(ResourceNotFoundException.class);
        participantService.findById(2L);
    }

    @Test
    public void shouldCreateParticipant() {
        var createParticipantDto = getCreateParticipantDto();
        participantService.createParticipant(createParticipantDto);
        var participant = participantRepository.findByFullNameAndBirthDate(
                createParticipantDto.getFullName(), createParticipantDto.getBirthDate())
                .orElseThrow(() -> new ResourceNotFoundException(1L, Participant.class));
        assertNotNull(participant);
        assertNotNull(participant.getId());
        assertEquals(participant.getFullName(), createParticipantDto.getFullName());
        assertEquals(participant.getBirthDate(), createParticipantDto.getBirthDate());
    }

    @Test
    public void shouldFindAllParticipants() {
        var page = PageRequest.of(0, 10);
        var participants = participantService.allParticipants(page);
        assertThat(participants.getContent().size(), is(1));
    }

    private CreateParticipantDto getCreateParticipantDto() {
        return CreateParticipantDto.builder()
                .fullName("Full2 Name2")
                .birthDate(LocalDate.parse("2000-02-02"))
                .build();
    }
}
