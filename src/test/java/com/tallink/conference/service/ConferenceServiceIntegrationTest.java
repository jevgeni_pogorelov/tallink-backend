package com.tallink.conference.service;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.tallink.conference.BaseIntegrationTest;
import com.tallink.conference.DBIntegrationTest;
import com.tallink.conference.exception.ConferenceAlreadyExistsException;
import com.tallink.conference.exception.ResourceNotFoundException;
import com.tallink.conference.model.Conference;
import com.tallink.conference.model.dto.CreateConferenceDto;
import com.tallink.conference.repository.ConferenceRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

import java.time.LocalDate;
import java.time.LocalTime;

import static com.tallink.conference.DataSetPaths.CONFERENCES;
import static com.tallink.conference.model.enums.ConferenceStatus.CANCELED;
import static com.tallink.conference.model.enums.ConferenceStatus.ROOM_ADDED;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@DBIntegrationTest
@DatabaseSetup(CONFERENCES)
public class ConferenceServiceIntegrationTest extends BaseIntegrationTest {

    @Autowired
    private ConferenceService conferenceService;
    @Autowired
    private ConferenceRepository conferenceRepository;

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void shouldFindById() {
        Long conferenceId = 1L;
        var conference = conferenceService.findById(conferenceId);
        assertThat(conference, notNullValue());
        assertEquals(conference.getId(), conferenceId);
        assertEquals(conference.getName(), "Conf name");
        assertEquals(conference.getDate(), LocalDate.parse("2020-01-01"));
        assertEquals(conference.getTime(), LocalTime.parse("01:01:00"));
        assertEquals(conference.getConferenceStatus(), ROOM_ADDED);
        assertThat(conference.getRoom(), notNullValue());
    }

    @Test
    public void shouldThrowsResourceNotFoundExceptionWhenFindConferenceById() {
        exceptionRule.expect(ResourceNotFoundException.class);
        conferenceService.findById(5L);
    }

    @Test
    public void shouldCreateConference() {
        var createConferenceDto = getCreateConferenceDto();
        conferenceService.createConference(createConferenceDto);
        var conference = conferenceRepository.findByName(createConferenceDto.getName())
                .orElseThrow(() -> new ResourceNotFoundException(1L, Conference.class));
        assertNotNull(conference);
        assertNotNull(conference.getId());
        assertEquals(conference.getName(), createConferenceDto.getName());
        assertEquals(conference.getDate(), createConferenceDto.getDate());
        assertEquals(conference.getTime(), createConferenceDto.getTime());
    }

    @Test
    public void shouldThrowsConferenceAlreadyExistsException() {
        exceptionRule.expect(ConferenceAlreadyExistsException.class);
        var createConferenceDto = getCreateConferenceDto();
        createConferenceDto.setName("Conf name");
        conferenceService.createConference(createConferenceDto);
    }

    @Test
    public void shouldFindAllConferences() {
        var page = PageRequest.of(0, 10);
        var conferences = conferenceService.allConferences(page);
        assertThat(conferences.getContent().size(), is(3));
    }

    @Test
    public void shouldCancelConference() {
        Long conferenceId = 1L;
        conferenceService.cancelConference(conferenceId);
        var conference = conferenceService.findById(conferenceId);
        assertNotNull(conference);
        assertEquals(conference.getConferenceStatus(), CANCELED);
    }

    private CreateConferenceDto getCreateConferenceDto() {
        return CreateConferenceDto.builder()
                .name("My Conf Name")
                .date(LocalDate.parse("2020-03-03"))
                .time(LocalTime.parse("10:00:00"))
                .build();
    }
}
