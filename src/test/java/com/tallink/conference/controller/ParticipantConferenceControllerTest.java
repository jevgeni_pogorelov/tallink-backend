package com.tallink.conference.controller;

import com.tallink.conference.model.dto.AddParticipantToConferenceDto;
import com.tallink.conference.service.ParticipantConferenceService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_NO_CONTENT;

class ParticipantConferenceControllerTest extends BaseControllerTest {

    private static final String ADD_PARTICIPANT_TO_CONFERENCE_URL = "/api/participantConference/addToConference";
    private static final String REMOVE_PARTICIPANT_FROM_CONFERENCE_URL = "/api/participantConference/removeFromConference";

    @MockBean
    private ParticipantConferenceService participantConferenceService;

    @Test
    public void addParticipantToConference() {
        given()
                .contentType("application/json")
                .body(asJsonString(getAddParticipantToConferenceDto()))
                .port(getPort())
        .when()
                .post(ADD_PARTICIPANT_TO_CONFERENCE_URL)
        .then()
                .statusCode(SC_NO_CONTENT);
    }

    @Test
    public void removeParticipantFromConference() {
        given()
                .port(getPort())
                .param("participantId", 1L)
                .param("conferenceId", 1L)
                .when()
                .delete(REMOVE_PARTICIPANT_FROM_CONFERENCE_URL)
                .then()
                .statusCode(SC_NO_CONTENT);
    }

    private AddParticipantToConferenceDto getAddParticipantToConferenceDto() {
        return AddParticipantToConferenceDto.builder()
                .conferenceId(1L)
                .participantId(1L)
                .build();
    }
}
