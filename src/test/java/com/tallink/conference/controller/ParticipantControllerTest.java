package com.tallink.conference.controller;

import com.tallink.conference.controller.dto.ParticipantDto;
import com.tallink.conference.model.dto.CreateParticipantDto;
import com.tallink.conference.service.ParticipantService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.Collections;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.apache.http.HttpStatus.SC_NO_CONTENT;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ParticipantControllerTest extends BaseControllerTest {

    private static final String CREATE_PARTICIPANT_URL = "/api/participant";
    private static final String GET_ALL_PARTICIPANTS_URL = "/api/participant";

    @MockBean
    private ParticipantService participantService;

    @Test
    public void createParticipant() {
        given()
                .contentType("application/json")
                .body(asJsonString(getNewParticipantDto()))
                .port(getPort())
        .when()
                .post(CREATE_PARTICIPANT_URL)
        .then()
                .statusCode(SC_NO_CONTENT);
    }

    @Test
    public void allParticipants() {
        when(participantService.allParticipants(any(Pageable.class)))
                .thenReturn(new PageImpl<>(Collections.singletonList(getMockParticipantDto()), PageRequest.of(0, 10), 1L));
        given()
                .port(getPort())
        .when()
                .get(GET_ALL_PARTICIPANTS_URL)
        .then()
                .statusCode(SC_OK)
                .contentType(JSON)
                .body("content", hasSize(1),
                        "content[0].fullName", equalTo("Full Name"),
                        "content[0].birthDate", equalTo("2000-01-01"));
    }

    private CreateParticipantDto getNewParticipantDto() {
        return CreateParticipantDto.builder()
                .fullName("Full Name")
                .birthDate(LocalDate.parse("2000-01-01"))
                .build();
    }

    private ParticipantDto getMockParticipantDto() {
        return ParticipantDto.builder()
                .id(1L)
                .fullName("Full Name")
                .birthDate(LocalDate.parse("2000-01-01"))
                .build();
    }

}
