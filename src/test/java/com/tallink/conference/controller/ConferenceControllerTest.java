package com.tallink.conference.controller;

import com.tallink.conference.controller.dto.ConferenceDto;
import com.tallink.conference.model.dto.CreateConferenceDto;
import com.tallink.conference.model.dto.UpdateConferenceDto;
import com.tallink.conference.service.ConferenceService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collections;

import static com.tallink.conference.model.enums.ConferenceStatus.NEW;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.apache.http.HttpStatus.SC_NO_CONTENT;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ConferenceControllerTest extends BaseControllerTest {

    private static final String CREATE_CONFERENCE_URL = "/api/conference";
    private static final String UPDATE_CONFERENCE_URL = "/api/conference/update";
    private static final String GET_ALL_CONFERENCES_URL = "/api/conference";
    private static final String CANCEL_CONFERENCE_URL = "/api/conference/cancel/{conferenceId}";

    @MockBean
    private ConferenceService conferenceService;

    @Test
    public void createConference() {
        given()
                .contentType("application/json")
                .body(asJsonString(getNewConferenceDto()))
                .port(getPort())
        .when()
                .post(CREATE_CONFERENCE_URL)
        .then()
                .statusCode(SC_NO_CONTENT);
    }

    @Test
    public void updateConference() {
        given()
                .contentType("application/json")
                .body(asJsonString(getUpdateConferenceDto()))
                .port(getPort())
        .when()
                .post(UPDATE_CONFERENCE_URL)
        .then()
                .statusCode(SC_NO_CONTENT);

    }

    @Test
    public void allConferences() {
        when(conferenceService.allConferences(any(Pageable.class))).
                thenReturn(new PageImpl<>(Collections.singletonList(getMockConferenceDto()), PageRequest.of(0, 10), 1L));
        given()
                .port(getPort())
        .when()
                .get(GET_ALL_CONFERENCES_URL)
        .then()
                .statusCode(SC_OK)
                .contentType(JSON)
                .body("content", hasSize(1),
                        "content[0].name", equalTo("Name"),
                        "content[0].date", equalTo("2020-01-01"),
                        "content[0].time", equalTo("01:01:00"),
                        "content[0].conferenceStatus", equalTo("NEW"));

    }

    @Test
    public void cancelConference() {
        given()
                .port(getPort())
                .pathParam("conferenceId", 1L)
        .when()
                .post(CANCEL_CONFERENCE_URL)
        .then()
                .statusCode(SC_NO_CONTENT);

    }

    private CreateConferenceDto getNewConferenceDto() {
        return CreateConferenceDto.builder()
                .name("Name")
                .date(LocalDate.parse("2020-01-01"))
                .time(LocalTime.parse("01:01:00"))
                .build();
    }

    private UpdateConferenceDto getUpdateConferenceDto() {
        return UpdateConferenceDto.builder()
                .id(1L)
                .name("Name")
                .date(LocalDate.parse("2020-01-01"))
                .time(LocalTime.parse("01:01:00"))
                .build();
    }

    private ConferenceDto getMockConferenceDto() {
        return ConferenceDto.builder()
                .id(1L)
                .name("Name")
                .date(LocalDate.parse("2020-01-01"))
                .time(LocalTime.parse("01:01:00"))
                .conferenceStatus(NEW)
                .build();
    }

}
