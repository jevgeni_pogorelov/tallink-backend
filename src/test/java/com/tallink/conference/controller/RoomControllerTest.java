package com.tallink.conference.controller;

import com.tallink.conference.controller.dto.RoomDto;
import com.tallink.conference.model.dto.CreateRoomDto;
import com.tallink.conference.model.dto.UpdateRoomDto;
import com.tallink.conference.service.RoomService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static java.util.Collections.singletonList;
import static org.apache.http.HttpStatus.SC_NO_CONTENT;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class RoomControllerTest extends BaseControllerTest {

    private static final String CREATE_ROOM_URL = "/api/room";
    private static final String UPDATE_ROOM_URL = "/api/room/update";
    private static final String GET_ALL_ROOMS_URL = "/api/room";
    private static final String FIND_AVAILABLE_ROOM_URL = "/api/room/{conferenceId}";

    @MockBean
    private RoomService roomService;

    @Test
    public void createRoom() {
        given()
                .contentType("application/json")
                .body(asJsonString(getNewRoomDto()))
                .port(getPort())
        .when()
                .post(CREATE_ROOM_URL)
        .then()
                .statusCode(SC_NO_CONTENT);
    }

    @Test
    public void updateRoom() {
        given()
                .contentType("application/json")
                .body(asJsonString(getUpdateRoomDto()))
                .port(getPort())
        .when()
                .post(UPDATE_ROOM_URL)
        .then()
                .statusCode(SC_NO_CONTENT);
    }

    @Test
    public void allRooms() {
        when(roomService.findAllRooms(any(Pageable.class))).
                thenReturn(new PageImpl<>(singletonList(getMockRoomDto()), PageRequest.of(0, 10), 1L));
        given()
                .port(getPort())
        .when()
                .get(GET_ALL_ROOMS_URL)
        .then()
                .statusCode(SC_OK)
                .contentType(JSON)
                .body("content", hasSize(1),
                        "content[0].name", equalTo("Name"),
                        "content[0].location", equalTo("Location"),
                        "content[0].maxSeats", equalTo(100));
    }

    @Test
    public void findAvailableRoomByConferenceId() {
        when(roomService.findAvailableRoomByConferenceId(1L)).
                thenReturn(singletonList(getMockRoomDto()));
        given()
                .port(getPort())
                .pathParam("conferenceId", 1L)
        .when()
                .get(FIND_AVAILABLE_ROOM_URL)
        .then()
                .statusCode(SC_OK)
                .contentType(JSON)
                .body("content", hasSize(1));
    }

    private CreateRoomDto getNewRoomDto() {
        return CreateRoomDto.builder()
                .location("Location")
                .name("Name")
                .maxSeats(100)
                .build();
    }

    private UpdateRoomDto getUpdateRoomDto() {
        return UpdateRoomDto.builder()
                .id(1L)
                .Location("Location")
                .name("Name")
                .maxSeats(100)
                .build();
    }

    private RoomDto getMockRoomDto() {
        return RoomDto.builder()
                .id(1L)
                .location("Location")
                .name("Name")
                .maxSeats(100)
                .build();
    }
}
