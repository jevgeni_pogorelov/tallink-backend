package com.tallink.conference.controller;

import com.tallink.conference.exception.*;
import com.tallink.conference.model.Conference;
import com.tallink.conference.model.dto.CreateConferenceDto;
import com.tallink.conference.model.dto.CreateParticipantDto;
import com.tallink.conference.model.dto.CreateRoomDto;
import com.tallink.conference.model.dto.UpdateConferenceDto;
import com.tallink.conference.service.ConferenceService;
import com.tallink.conference.service.ParticipantService;
import com.tallink.conference.service.RoomService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.time.LocalTime;

import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_CONFLICT;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doThrow;


class AppWebErrorHandlerTest extends BaseControllerTest {

    private static final String CREATE_ROOM_URL = "/api/room";
    private static final String FIND_AVAILABLE_ROOM_URL = "/api/room/{conferenceId}";
    private static final String CREATE_CONFERENCE_URL = "/api/conference";
    private static final String UPDATE_CONFERENCE_URL = "/api/conference/update";
    private static final String CREATE_PARTICIPANT_URL = "/api/participant";

    @MockBean
    private ConferenceService conferenceService;

    @MockBean
    private ParticipantService participantService;

    @MockBean
    private RoomService roomService;

    @Test
    public void handleRoomAlreadyExistsException() {
        doThrow(new RoomAlreadyExistsException("Room Name"))
                .when(roomService).createRoom(any(CreateRoomDto.class));
        given()
                .contentType("application/json")
                .body(asJsonString(getNewRoomDto()))
                .port(getPort())
        .when()
                .post(CREATE_ROOM_URL)
        .then()
                .statusCode(SC_CONFLICT)
                .body("title", equalTo("Room already exists"),
                        "details", equalTo("Room with name Room Name already exists"),
                        "code", equalTo("ROOM_ALREADY_EXISTS"));
    }

    @Test
    public void handleResourceNotFoundException() {
        doThrow(new ResourceNotFoundException(1L, Conference.class))
                .when(conferenceService).updateConference(any(UpdateConferenceDto.class));
        given()
                .contentType("application/json")
                .body(asJsonString(getUpdateConferenceDto()))
                .port(getPort())
        .when()
                .post(UPDATE_CONFERENCE_URL)
        .then()
                .statusCode(SC_NOT_FOUND)
                .body("title", equalTo("Resource not found"),
                        "details", equalTo("The 'Conference' entity with id='1' was not found!"),
                        "code", equalTo("RESOURCE_NOT_FOUND"));
    }

    @Test
    public void handleNoAvailableRoomException() {
        doThrow(new NoAvailableRoomException(100, LocalDate.parse("2020-03-03")))
                .when(roomService).findAvailableRoomByConferenceId(anyLong());
        given()
                .port(getPort())
                .pathParam("conferenceId", 1L)
        .when()
                .get(FIND_AVAILABLE_ROOM_URL)
        .then()
                .statusCode(SC_NOT_FOUND)
                .body("title", equalTo("No Available Room"),
                        "details", equalTo("There are no available room for 100 participants at 2020-03-03 date."),
                        "code", equalTo("NO_AVAILABLE_ROOM"));

    }

    @Test
    public void handleConferenceAlreadyExistsException() {
        doThrow(new ConferenceAlreadyExistsException("Vue Conf"))
                .when(conferenceService).createConference(any(CreateConferenceDto.class));
        given()
                .contentType("application/json")
                .body(asJsonString(getNewConferenceDto()))
                .port(getPort())
        .when()
                .post(CREATE_CONFERENCE_URL)
        .then()
                .statusCode(SC_CONFLICT)
                .body("title", equalTo("Conference already exists"),
                        "details", equalTo("Conference with name Vue Conf already exists"),
                        "code", equalTo("CONFERENCE_ALREADY_EXISTS"));
    }

    @Test
    public void handleParticipantAlreadyExistsException() {
        doThrow(new ParticipantAlreadyExistsException("Full Name", LocalDate.parse("2000-01-01")))
                .when(participantService).createParticipant(any(CreateParticipantDto.class));
        given()
                .contentType("application/json")
                .body(asJsonString(getNewParticipantDto()))
                .port(getPort())
        .when()
                .post(CREATE_PARTICIPANT_URL)
        .then()
                .statusCode(SC_CONFLICT)
                .body("title", equalTo("Participant already exists"),
                        "details", equalTo("Participant with name Full Name and birth date 2000-01-01 already exists"),
                        "code", equalTo("PARTICIPANT_ALREADY_EXISTS"));
    }

    private CreateConferenceDto getNewConferenceDto() {
        return CreateConferenceDto.builder()
                .name("Name")
                .date(LocalDate.parse("2020-01-01"))
                .time(LocalTime.parse("01:01:00"))
                .build();
    }

    private CreateParticipantDto getNewParticipantDto() {
        return CreateParticipantDto.builder()
                .fullName("Full Name")
                .birthDate(LocalDate.parse("2000-01-01"))
                .build();
    }

    private UpdateConferenceDto getUpdateConferenceDto() {
        return UpdateConferenceDto.builder()
                .id(1L)
                .name("Name")
                .date(LocalDate.parse("2020-01-01"))
                .time(LocalTime.parse("01:01:00"))
                .build();
    }

    private CreateRoomDto getNewRoomDto() {
        return CreateRoomDto.builder()
                .location("Location")
                .name("Name")
                .maxSeats(100)
                .build();
    }


}
