package com.tallink.conference;

import org.junit.runner.RunWith;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static com.tallink.conference.AppConstants.TEST_PROPERTIES;

@RunWith(SpringRunner.class)
@Transactional
@TestPropertySource(TEST_PROPERTIES)
public abstract class BaseIntegrationTest {
}
