package com.tallink.conference;

import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.boot.test.mock.mockito.ResetMocksTestExecutionListener;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.tallink.conference.AppConstants.PROFILE_TEST;

@Retention(RetentionPolicy.RUNTIME)
@ActiveProfiles(PROFILE_TEST)
@SpringBootTest
@TestExecutionListeners({ TransactionDbUnitTestExecutionListener.class,
        ResetMocksTestExecutionListener.class,
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        MockitoTestExecutionListener.class})
public @interface DBIntegrationTest {
}
