package com.tallink.conference;

public interface DataSetPaths {
    String DBUNIT = "/dbunit";
    String ROOMS = DBUNIT + "/rooms.xml";
    String PARTICIPANTS = DBUNIT + "/participants.xml";
    String CONFERENCES = DBUNIT + "/conferences.xml";
}
