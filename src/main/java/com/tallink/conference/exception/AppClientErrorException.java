package com.tallink.conference.exception;

import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

public class AppClientErrorException extends AppBusinessException {

    private static final long serialVersionUID = 3112981996624354917L;

    public AppClientErrorException(final AppExceptionReason reason, final Object... parameters) {
        this(reason, BAD_REQUEST, parameters);
    }

    AppClientErrorException(final AppExceptionReason reason, final HttpStatus httpStatus, final Object... parameters) {
        super(reason.getCode(), reason.getMessageKey(), httpStatus, parameters);
    }
}
