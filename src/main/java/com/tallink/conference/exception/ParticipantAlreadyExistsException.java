package com.tallink.conference.exception;

import java.time.LocalDate;

import static com.tallink.conference.exception.CommonAppExceptionReason.PARTICIPANT_ALREADY_EXISTS;
import static org.springframework.http.HttpStatus.CONFLICT;

public class ParticipantAlreadyExistsException extends AppClientErrorException {

    private static final long serialVersionUID = 5861310537366287163L;

    public ParticipantAlreadyExistsException(String fullName, LocalDate birthDate) {
        super(PARTICIPANT_ALREADY_EXISTS, CONFLICT, fullName, birthDate);
    }
}
