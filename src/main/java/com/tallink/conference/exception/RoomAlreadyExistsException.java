package com.tallink.conference.exception;

import static com.tallink.conference.exception.CommonAppExceptionReason.ROOM_ALREADY_EXISTS;
import static org.springframework.http.HttpStatus.CONFLICT;

public class RoomAlreadyExistsException extends AppClientErrorException {

    private static final long serialVersionUID = 5861310537366287163L;

    public RoomAlreadyExistsException(String name) {
        super(ROOM_ALREADY_EXISTS, CONFLICT, name);
    }
}
