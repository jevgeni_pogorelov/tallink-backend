package com.tallink.conference.exception;

import static com.tallink.conference.exception.CommonAppExceptionReason.CONFERENCE_ALREADY_EXISTS;
import static org.springframework.http.HttpStatus.CONFLICT;

public class ConferenceAlreadyExistsException extends AppClientErrorException {

    private static final long serialVersionUID = 5861310537366287163L;

    public ConferenceAlreadyExistsException(String name) {
        super(CONFERENCE_ALREADY_EXISTS, CONFLICT, name);
    }
}
