package com.tallink.conference.exception;

public interface AppExceptionReason {

    String getCode();

    String getMessageKey();
}
