package com.tallink.conference.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

import static com.tallink.conference.exception.CommonAppExceptionReason.RESOURCE_NOT_FOUND;
import static java.util.Optional.ofNullable;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@ResponseStatus(value = NOT_FOUND, reason = "Resource Not Found")
public class ResourceNotFoundException extends AppClientErrorException {

    private static final long serialVersionUID = 6930651518569501043L;

    public <T> ResourceNotFoundException(final T resourceId, final Class<?> resourceClass) {
        this(ofNullable(resourceId).map(Object::toString).orElse(""),
                ofNullable(resourceClass).map(Class::getSimpleName).orElse("Unknown"));
    }

    private ResourceNotFoundException(final String resourceId, final String resourceClassName) {
        super(RESOURCE_NOT_FOUND, NOT_FOUND, resourceClassName, resourceId);
    }
}
