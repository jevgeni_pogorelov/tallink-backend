package com.tallink.conference.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum CommonAppExceptionReason implements AppExceptionReason {

    ROOM_ALREADY_EXISTS("app.room.already.exists"),
    RESOURCE_NOT_FOUND("app.resource.not.found"),
    NO_AVAILABLE_ROOM("app.no.available.room"),
    CONFERENCE_ALREADY_EXISTS("app.conference.already.exists"),
    PARTICIPANT_ALREADY_EXISTS("app.participant.already.exists");

    @Getter
    private final String messageKey;

    @Override
    public String getCode() {
        return name();
    }
}
