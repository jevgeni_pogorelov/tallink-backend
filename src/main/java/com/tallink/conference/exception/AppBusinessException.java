package com.tallink.conference.exception;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.springframework.http.HttpStatus;

import java.util.Arrays;

public class AppBusinessException extends AppException {

    private static final long serialVersionUID = -5572487885355839897L;

    private final String code;
    private final String messageKey;
    private final Object[] parameters;
    private final HttpStatus httpStatus;

    public AppBusinessException(String code, String messageKey, HttpStatus httpStatus, Object... parameters) {
        super("App business error: " + code);
        this.code = code;
        this.messageKey = messageKey;
        this.parameters = parameters;
        this.httpStatus = httpStatus;
    }

    @Override
    public String getMessage() {
        return toString();
    }

    @Override
    public String getLocalizedMessage() {
        return toString();
    }

    @SuppressFBWarnings("EI_EXPOSE_REP")
    public Object[] getParameters() {
        return parameters;
    }

    @Override
    public String toString() {
        return "AppBusinessException [errorCode=" + code + ", parameters=" + Arrays.toString(parameters) + ", httpStatus=" + httpStatus + "]";
    }

    public String getErrorCode() {
        return code;
    }

    public String getMessageKey() {
        return messageKey;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
