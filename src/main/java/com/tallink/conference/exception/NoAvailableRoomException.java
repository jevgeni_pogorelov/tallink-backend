package com.tallink.conference.exception;

import java.time.LocalDate;

import static com.tallink.conference.exception.CommonAppExceptionReason.NO_AVAILABLE_ROOM;
import static org.springframework.http.HttpStatus.NOT_FOUND;

public class NoAvailableRoomException extends AppClientErrorException {

    private static final long serialVersionUID = 5861310537366287163L;

    public NoAvailableRoomException(int numberOfParticipants, LocalDate conferenceDate) {
        super(NO_AVAILABLE_ROOM, NOT_FOUND, numberOfParticipants, conferenceDate);
    }
}
