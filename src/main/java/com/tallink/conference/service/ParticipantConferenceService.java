package com.tallink.conference.service;

import com.tallink.conference.model.ParticipantConference;
import com.tallink.conference.model.dto.AddParticipantToConferenceDto;
import com.tallink.conference.repository.ParticipantConferenceRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class ParticipantConferenceService {

    private final ParticipantService participantService;
    private final ConferenceService conferenceService;
    private final ParticipantConferenceRepository participantConferenceRepository;

    public void addParticipantToConference(AddParticipantToConferenceDto addParticipantToConferenceDto) {
        var participant = participantService.findById(addParticipantToConferenceDto.getParticipantId());
        var conference = conferenceService.findById(addParticipantToConferenceDto.getConferenceId());
        var participantConference = ParticipantConference.builder()
                .conference(conference)
                .participant(participant)
                .build();
        participantConferenceRepository.save(participantConference);
    }

    public void removeParticipantFromConference(Long participantId, Long conferenceId) {
        participantConferenceRepository.deleteByConference_IdAndParticipant_Id(conferenceId, participantId);
    }
}
