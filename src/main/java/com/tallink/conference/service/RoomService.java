package com.tallink.conference.service;

import com.tallink.conference.controller.dto.RoomDto;
import com.tallink.conference.exception.ResourceNotFoundException;
import com.tallink.conference.exception.RoomAlreadyExistsException;
import com.tallink.conference.model.Room;
import com.tallink.conference.model.dto.CreateRoomDto;
import com.tallink.conference.model.dto.UpdateRoomDto;
import com.tallink.conference.repository.room.RoomRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.dozer.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.tallink.conference.util.Utils.nullSafeStream;
import static java.util.stream.Collectors.toList;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class RoomService {


    private final Mapper mapper;
    private final CommonRoomConferenceService commonRoomConferenceService;
    private final RoomRepository roomRepository;

    public void createRoom(CreateRoomDto createRoomDto) {
        if (roomExists(createRoomDto)) {
            throw new RoomAlreadyExistsException(createRoomDto.getName());
        }
        var room = mapper.map(createRoomDto, Room.class);
        roomRepository.save(room);

    }

    public void updateRoom(UpdateRoomDto updateRoomDto) {
        var foundedRoom = roomRepository.findByName(updateRoomDto.getName());
        foundedRoom.ifPresent(room -> {
            if (!room.getId().equals(updateRoomDto.getId())) {
                throw new RoomAlreadyExistsException(updateRoomDto.getName());
            }
        });

        var room = roomRepository.findById(updateRoomDto.getId())
                .orElseThrow(() -> new ResourceNotFoundException(updateRoomDto.getId(), Room.class));
        room.setName(updateRoomDto.getName());
        room.setLocation(updateRoomDto.getLocation());
        room.setMaxSeats(updateRoomDto.getMaxSeats());
        roomRepository.save(room);
    }

    @Transactional(readOnly = true)
    public Page<RoomDto> findAllRooms(Pageable pageable) {
        return roomRepository.findAll(pageable)
                .map(room -> mapper.map(room, RoomDto.class));
    }

    @Transactional(readOnly = true)
    public List<RoomDto> findAvailableRoomByConferenceId(Long conferenceId) {
        var conference = commonRoomConferenceService.findConferenceById(conferenceId);
        return nullSafeStream(roomRepository.availableRoom(conference.getParticipants().size(),
                conference.getDate()))
                .map(room -> mapper.map(room, RoomDto.class))
                .collect(toList());
    }

    private boolean roomExists(CreateRoomDto createRoomDto) {
        return roomRepository.findByName(createRoomDto.getName()).isPresent();
    }
}
