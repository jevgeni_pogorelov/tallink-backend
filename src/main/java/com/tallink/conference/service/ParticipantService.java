package com.tallink.conference.service;

import com.tallink.conference.controller.dto.ParticipantDetailDto;
import com.tallink.conference.controller.dto.ParticipantDto;
import com.tallink.conference.exception.ParticipantAlreadyExistsException;
import com.tallink.conference.exception.ResourceNotFoundException;
import com.tallink.conference.model.Participant;
import com.tallink.conference.model.dto.CreateParticipantDto;
import com.tallink.conference.repository.ParticipantRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.dozer.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class ParticipantService {

    private final Mapper mapper;
    private final ParticipantRepository participantRepository;

    @Transactional(readOnly = true)
    public Participant findById(Long id) {
        return participantRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(id, Participant.class));
    }

    public void createParticipant(CreateParticipantDto createParticipantDto) {
        if (participantExists(createParticipantDto)) {
            throw new ParticipantAlreadyExistsException(createParticipantDto.getFullName(),
                    createParticipantDto.getBirthDate());
        }
        var participant = mapper.map(createParticipantDto, Participant.class);
        participantRepository.save(participant);
    }

    @Transactional(readOnly = true)
    public Page<ParticipantDto> allParticipants(Pageable pageable) {
        return participantRepository.findAll(pageable)
                .map(participant -> mapper.map(participant, ParticipantDto.class));
    }

    public ParticipantDetailDto getAllParticipantConferences(Long participantId) {
        var participant = findById(participantId);
        return mapper.map(participant, ParticipantDetailDto.class);
    }

    private boolean participantExists(CreateParticipantDto createParticipantDto) {
        return participantRepository.findByFullNameAndBirthDate(createParticipantDto.getFullName(),
                createParticipantDto.getBirthDate()).isPresent();
    }
}
