package com.tallink.conference.service;

import com.tallink.conference.exception.ResourceNotFoundException;
import com.tallink.conference.model.Conference;
import com.tallink.conference.model.Room;
import com.tallink.conference.repository.ConferenceRepository;
import com.tallink.conference.repository.room.RoomRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class CommonRoomConferenceService {

    private final ConferenceRepository conferenceRepository;
    private final RoomRepository roomRepository;

    @Transactional(readOnly = true)
    public Conference findConferenceById(Long id) {
        return conferenceRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(id, Conference.class));
    }

    @Transactional(readOnly = true)
    public Room findRoomById(Long id) {
        return roomRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(id, Room.class));
    }

}
