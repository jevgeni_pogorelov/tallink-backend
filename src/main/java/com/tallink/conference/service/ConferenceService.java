package com.tallink.conference.service;

import com.tallink.conference.controller.dto.ConferenceDto;
import com.tallink.conference.exception.ConferenceAlreadyExistsException;
import com.tallink.conference.exception.ResourceNotFoundException;
import com.tallink.conference.model.Conference;
import com.tallink.conference.model.dto.AddRoomToConferenceDto;
import com.tallink.conference.model.dto.CreateConferenceDto;
import com.tallink.conference.model.dto.UpdateConferenceDto;
import com.tallink.conference.repository.ConferenceRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.dozer.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.tallink.conference.model.enums.ConferenceStatus.CANCELED;
import static com.tallink.conference.model.enums.ConferenceStatus.ROOM_ADDED;
import static com.tallink.conference.repository.predicate.ConferencePredicate.searchAvailableConferences;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class ConferenceService {

    private final Mapper mapper;
    private final CommonRoomConferenceService commonRoomConferenceService;
    private final ConferenceRepository conferenceRepository;

    @Transactional(readOnly = true)
    public Conference findById(Long id) {
        return conferenceRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(id, Conference.class));
    }

    public void createConference(CreateConferenceDto createConferenceDto) {
        if (conferenceExists(createConferenceDto)) {
            throw new ConferenceAlreadyExistsException(createConferenceDto.getName());
        }
        var conference = mapper.map(createConferenceDto, Conference.class);
        conferenceRepository.save(conference);
    }

    public void updateConference(UpdateConferenceDto updateConferenceDto) {

        var foundedConference = conferenceRepository.findByName(updateConferenceDto.getName());

        foundedConference.ifPresent(conference -> {
            if (!conference.getId().equals(updateConferenceDto.getId())) {
                throw new ConferenceAlreadyExistsException(updateConferenceDto.getName());
            }
        });

        var conference = conferenceRepository.findById(updateConferenceDto.getId())
                .orElseThrow(() -> new ResourceNotFoundException(updateConferenceDto.getId(), Conference.class));
        conference.setName(updateConferenceDto.getName());
        conference.setDate(updateConferenceDto.getDate());
        conference.setTime(updateConferenceDto.getTime());
        conferenceRepository.save(conference);
    }

    @Transactional(readOnly = true)
    public Page<ConferenceDto> allConferences(Pageable pageable) {
        return conferenceRepository.findAll(pageable)
                .map(conference -> mapper.map(conference, ConferenceDto.class));
    }

    public void cancelConference(Long conferenceId) {
        var conference = this.findById(conferenceId);
        conference.setConferenceStatus(CANCELED);
        conference.setRoom(null);
        conferenceRepository.save(conference);
    }

    public void addRoomToConference(AddRoomToConferenceDto addRoomToConferenceDto) {
        var room = commonRoomConferenceService.findRoomById(addRoomToConferenceDto.getRoomId());
        var conference = this.findById(addRoomToConferenceDto.getConferenceId());
        conference.setRoom(room);
        conference.setConferenceStatus(ROOM_ADDED);
        conferenceRepository.save(conference);
    }

    public Page<ConferenceDto> findAvailableConferencesToParticipate(Pageable pageable, Long participantId) {
        return conferenceRepository.findAll(searchAvailableConferences(participantId), pageable)
                .map(conference -> mapper.map(conference, ConferenceDto.class));
    }

    private boolean conferenceExists(CreateConferenceDto createConferenceDto) {
        return conferenceRepository.findByName(createConferenceDto.getName()).isPresent();
    }
}
