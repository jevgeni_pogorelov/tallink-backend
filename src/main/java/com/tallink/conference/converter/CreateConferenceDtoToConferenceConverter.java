package com.tallink.conference.converter;

import com.tallink.conference.dozer.AbstractConverter;
import com.tallink.conference.model.Conference;
import com.tallink.conference.model.dto.CreateConferenceDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static com.tallink.conference.model.enums.ConferenceStatus.NEW;

@Component
@Slf4j
public class CreateConferenceDtoToConferenceConverter extends AbstractConverter<CreateConferenceDto, Conference> {

    public CreateConferenceDtoToConferenceConverter() {
        super(CreateConferenceDto.class, Conference.class);
    }

    @Override
    public Class<CreateConferenceDto> getSourceClass() {
        return CreateConferenceDto.class;
    }

    @Override
    public Class<Conference> getDestinationClass() {
        return Conference.class;
    }

    @Override
    public Conference convertTo(CreateConferenceDto source, Conference destination) {

        return Conference.builder()
                .name(source.getName())
                .date(source.getDate())
                .time(source.getTime())
                .conferenceStatus(NEW)
                .build();
    }

    @Override
    public CreateConferenceDto convertFrom(Conference conference, CreateConferenceDto createConferenceDto) {
        throw new UnsupportedOperationException();
    }
}
