package com.tallink.conference.converter;

import com.tallink.conference.controller.dto.ConferenceDto;
import com.tallink.conference.controller.dto.ParticipantDetailDto;
import com.tallink.conference.dozer.AbstractConverter;
import com.tallink.conference.model.Participant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Objects;

import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

@Component
@Slf4j
public class ParticipantToParticipantDetailDtoConverter extends AbstractConverter<Participant, ParticipantDetailDto> {

    public ParticipantToParticipantDetailDtoConverter() {
        super(Participant.class, ParticipantDetailDto.class);
    }

    @Override
    public Class<Participant> getSourceClass() {
        return Participant.class;
    }

    @Override
    public Class<ParticipantDetailDto> getDestinationClass() {
        return ParticipantDetailDto.class;
    }

    @Override
    public ParticipantDetailDto convertTo(Participant source, ParticipantDetailDto destination) {
        if (Objects.isNull(destination)) {
            destination = ParticipantDetailDto.builder().build();
        }

        destination.setId(source.getId());
        destination.setFullName(source.getFullName());
        destination.setBirthDate(source.getBirthDate());

        if (isNotEmpty(source.getConferences())) {
            destination.setConferences(source.getConferences().stream()
            .map(conference -> getMapper().map(conference, ConferenceDto.class))
            .collect(toList()));
        }

        return destination;
    }

    @Override
    public Participant convertFrom(ParticipantDetailDto participantDetailDto, Participant participant) {
        throw new UnsupportedOperationException();
    }
}
