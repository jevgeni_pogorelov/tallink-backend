package com.tallink.conference.converter;

import com.tallink.conference.controller.dto.RoomDto;
import com.tallink.conference.dozer.AbstractConverter;
import com.tallink.conference.model.Room;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@Slf4j
public class RoomToRoomDtoConverter extends AbstractConverter<Room, RoomDto> {

    public RoomToRoomDtoConverter() {
        super(Room.class, RoomDto.class);
    }

    @Override
    public Class<Room> getSourceClass() {
        return Room.class;
    }

    @Override
    public Class<RoomDto> getDestinationClass() {
        return RoomDto.class;
    }

    @Override
    public RoomDto convertTo(Room source, RoomDto destination) {

        if (Objects.isNull(destination)) {
            destination = RoomDto.builder().build();
        }

        destination.setId(source.getId());
        destination.setName(source.getName());
        destination.setLocation(source.getLocation());
        destination.setMaxSeats(source.getMaxSeats());

        return destination;
    }

    @Override
    public Room convertFrom(RoomDto roomDto, Room room) {
        throw new UnsupportedOperationException();
    }
}
