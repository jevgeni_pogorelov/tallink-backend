package com.tallink.conference.converter;

import com.tallink.conference.controller.dto.ParticipantDto;
import com.tallink.conference.dozer.AbstractConverter;
import com.tallink.conference.model.Participant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@Slf4j
public class ParticipantToParticipantDtoConverter extends AbstractConverter<Participant, ParticipantDto> {

    public ParticipantToParticipantDtoConverter() {
        super(Participant.class, ParticipantDto.class);
    }

    @Override
    public Class<Participant> getSourceClass() {
        return Participant.class;
    }

    @Override
    public Class<ParticipantDto> getDestinationClass() {
        return ParticipantDto.class;
    }

    @Override
    public ParticipantDto convertTo(Participant source, ParticipantDto destination) {
        if (Objects.isNull(destination)) {
            destination = ParticipantDto.builder().build();
        }
        destination.setId(source.getId());
        destination.setFullName(source.getFullName());
        destination.setBirthDate(source.getBirthDate());

        return destination;
    }

    @Override
    public Participant convertFrom(ParticipantDto participantDto, Participant participant) {
        throw new UnsupportedOperationException();
    }
}
