package com.tallink.conference.converter;

import com.tallink.conference.dozer.AbstractConverter;
import com.tallink.conference.model.Room;
import com.tallink.conference.model.dto.CreateRoomDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CreateRoomDtoToRoomConverter extends AbstractConverter<CreateRoomDto, Room> {

    public CreateRoomDtoToRoomConverter() {
        super(CreateRoomDto.class, Room.class);
    }

    @Override
    public Class<CreateRoomDto> getSourceClass() {
        return CreateRoomDto.class;
    }

    @Override
    public Class<Room> getDestinationClass() {
        return Room.class;
    }

    @Override
    public Room convertTo(CreateRoomDto source, Room destination) {

        return Room.builder()
                .name(source.getName())
                .location(source.getLocation())
                .maxSeats(source.getMaxSeats())
                .build();
    }

    @Override
    public CreateRoomDto convertFrom(Room room, CreateRoomDto createRoomDto) {
        throw new UnsupportedOperationException();
    }
}
