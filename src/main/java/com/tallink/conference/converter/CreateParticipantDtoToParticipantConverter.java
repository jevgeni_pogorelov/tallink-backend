package com.tallink.conference.converter;

import com.tallink.conference.dozer.AbstractConverter;
import com.tallink.conference.model.Participant;
import com.tallink.conference.model.dto.CreateParticipantDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CreateParticipantDtoToParticipantConverter extends AbstractConverter<CreateParticipantDto, Participant> {

    public CreateParticipantDtoToParticipantConverter() {
        super(CreateParticipantDto.class, Participant.class);
    }

    @Override
    public Class<CreateParticipantDto> getSourceClass() {
        return CreateParticipantDto.class;
    }

    @Override
    public Class<Participant> getDestinationClass() {
        return Participant.class;
    }

    @Override
    public Participant convertTo(CreateParticipantDto source, Participant destination) {
        return Participant.builder()
                .fullName(source.getFullName())
                .birthDate(source.getBirthDate())
                .build();
    }

    @Override
    public CreateParticipantDto convertFrom(Participant participant, CreateParticipantDto createParticipantDto) {
        throw new UnsupportedOperationException();
    }
}
