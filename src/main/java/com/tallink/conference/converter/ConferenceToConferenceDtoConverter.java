package com.tallink.conference.converter;

import com.tallink.conference.controller.dto.ConferenceDto;
import com.tallink.conference.controller.dto.RoomDto;
import com.tallink.conference.dozer.AbstractConverter;
import com.tallink.conference.model.Conference;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@Slf4j
public class ConferenceToConferenceDtoConverter extends AbstractConverter<Conference, ConferenceDto> {

    public ConferenceToConferenceDtoConverter() {
        super(Conference.class, ConferenceDto.class);
    }

    @Override
    public Class<Conference> getSourceClass() {
        return Conference.class;
    }

    @Override
    public Class<ConferenceDto> getDestinationClass() {
        return ConferenceDto.class;
    }

    @Override
    public ConferenceDto convertTo(Conference source, ConferenceDto destination) {
        if (Objects.isNull(destination)) {
            destination = ConferenceDto.builder().build();
        }

        destination.setId(source.getId());
        destination.setName(source.getName());
        destination.setDate(source.getDate());
        destination.setTime(source.getTime());
        if (source.getRoom() != null) {
            destination.setRoom(getMapper().map(source.getRoom(), RoomDto.class));
        }
        destination.setConferenceStatus(source.getConferenceStatus());
        if (source.getParticipants() != null) {
           destination.setNumberOfParticipants(source.getParticipants().size());
        }

        return destination;
    }

    @Override
    public Conference convertFrom(ConferenceDto conferenceDto, Conference conference) {
        throw new UnsupportedOperationException();
    }
}
