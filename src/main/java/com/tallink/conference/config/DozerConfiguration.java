package com.tallink.conference.config;

import com.tallink.conference.dozer.ZonedDateTimeConverter;
import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static java.util.Collections.singletonList;

@Configuration
public class DozerConfiguration {

    @Bean
    public DozerBeanMapperFactoryBean mapper() {
        var zonedDateTimeConverter = new ZonedDateTimeConverter();
        var beanMapper = new DozerBeanMapperFactoryBean();
        beanMapper.setCustomConverters(singletonList(zonedDateTimeConverter));
        return beanMapper;
    }
}
