package com.tallink.conference.config;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import static com.tallink.conference.AppSpringBeanNames.HIBERNATE_ERROR_MESSAGE_SOURCE_BEAN;

@Configuration
public class LocalizationConfig {

    @Bean
    public LocaleResolver localeResolver() {
        return new AcceptHeaderLocaleResolver();
    }

    @Bean(name = HIBERNATE_ERROR_MESSAGE_SOURCE_BEAN)
    public MessageSource hibernateErrorMessageSource() {
        var src = new ReloadableResourceBundleMessageSource();
        src.setBasenames("classpath:/org/hibernate/validator/ValidationMessages", "classpath:/messages/validation.messages");
        src.setDefaultEncoding("UTF-8");
        return src;
    }
}
