package com.tallink.conference;

public interface AppSpringBeanNames {
    String HIBERNATE_ERROR_MESSAGE_SOURCE_BEAN = "hibernate.error.message.source";
}
