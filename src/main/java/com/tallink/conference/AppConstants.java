package com.tallink.conference;

public interface AppConstants {
    String API_URI = "/api";
    String PROFILE_TEST = "test";
    String TEST_PROPERTIES = "classpath:test.properties";

}
