package com.tallink.conference;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class ConferenceManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConferenceManagementApplication.class, args);
	}

}
