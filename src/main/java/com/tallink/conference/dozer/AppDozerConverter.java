package com.tallink.conference.dozer;

import org.dozer.ConfigurableCustomConverter;

public interface AppDozerConverter<S, D> extends ConfigurableCustomConverter {

    Class<S> getSourceClass();

    Class<D> getDestinationClass();
}
