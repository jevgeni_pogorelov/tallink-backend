package com.tallink.conference.dozer;

import com.google.common.collect.ImmutableList;
import org.dozer.classmap.Configuration;
import org.dozer.classmap.CopyByReference;
import org.dozer.classmap.MappingFileData;
import org.dozer.converters.CustomConverterDescription;
import org.dozer.loader.api.BeanMappingBuilder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;

@Component
public class AppDozerBeanMappingBuilder extends BeanMappingBuilder {

    private List<? extends AppDozerConverter<?, ?>> converters;

    private final Configuration config = new Configuration();

    public AppDozerBeanMappingBuilder(List<? extends AppDozerConverter<?, ?>> converters) {
        this.converters = converters;
    }

    @PostConstruct
    private void init() {
        converters = converters == null ? Collections.emptyList() : converters;
    }

    @Override
    protected void configure() {

    }

    @Override
    public MappingFileData build() {
        var mappingFile = super.build();
        var copyByReferences = config.getCopyByReferences();
        copyByReferences.add(new CopyByReference("java.time.*"));
        registerConverters();
        mappingFile.setConfiguration(config);
        return mappingFile;
    }

    private void registerConverters() {
        getAllConverters().forEach(this::addConverter);
    }

    private void addConverter(AppDozerConverter<?, ?> dozerConverter) {
        var converterDescription = new CustomConverterDescription();
        converterDescription.setType(dozerConverter.getClass());
        converterDescription.setClassA(dozerConverter.getSourceClass());
        converterDescription.setClassB(dozerConverter.getDestinationClass());
        config.getCustomConverters().addConverter(converterDescription);
    }

    private List<AppDozerConverter<?, ?>> getAllConverters() {
        return ImmutableList.<AppDozerConverter<?, ?>>builder()
                .addAll(converters)
                .build();
    }
}
