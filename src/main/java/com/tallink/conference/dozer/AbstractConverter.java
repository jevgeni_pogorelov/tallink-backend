package com.tallink.conference.dozer;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.dozer.DozerConverter;
import org.dozer.Mapper;
import org.dozer.MapperAware;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public abstract class AbstractConverter <SOURCE, DESTINATION> extends DozerConverter<SOURCE, DESTINATION>
        implements AppDozerConverter<SOURCE, DESTINATION>, MapperAware {

    public AbstractConverter(Class<SOURCE> prototypeA, Class<DESTINATION> prototypeB) {
        super(prototypeA, prototypeB);
    }

    @Getter
    @Setter
    private Mapper mapper;
}
