package com.tallink.conference.repository;

import com.tallink.conference.model.ParticipantConference;
import org.springframework.stereotype.Repository;

@Repository
public interface ParticipantConferenceRepository extends BaseRepository<ParticipantConference> {

    void deleteByConference_IdAndParticipant_Id(Long conferenceId, Long participantId);

}
