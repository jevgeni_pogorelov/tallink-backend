package com.tallink.conference.repository;

import com.tallink.conference.model.AbstractEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BaseRepository<T extends AbstractEntity> extends JpaRepository<T, Long> {
}
