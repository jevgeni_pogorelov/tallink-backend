package com.tallink.conference.repository.room;

import com.tallink.conference.model.Room;

import java.time.LocalDate;
import java.util.List;

public interface RoomRepositoryCustom {

    List<Room> availableRoom(int numberOfParticipants, LocalDate conferenceDate);
}
