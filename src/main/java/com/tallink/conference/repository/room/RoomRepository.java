package com.tallink.conference.repository.room;

import com.tallink.conference.model.Room;
import com.tallink.conference.repository.BaseRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoomRepository extends BaseRepository<Room>, QuerydslPredicateExecutor<Room>, RoomRepositoryCustom {

    Optional<Room> findByName(String name);
}
