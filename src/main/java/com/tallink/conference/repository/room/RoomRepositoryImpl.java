package com.tallink.conference.repository.room;

import static com.tallink.conference.model.QRoom.room;
import static com.tallink.conference.model.QConference.conference;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.tallink.conference.model.Room;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
public class RoomRepositoryImpl implements RoomRepositoryCustom {

    private final JPAQueryFactory jpaQueryFactory;

    @Override
    public List<Room> availableRoom(int numberOfParticipants, LocalDate conferenceDate) {
        return jpaQueryFactory.select(room).distinct()
                .from(room)
                .where(room.maxSeats.goe(numberOfParticipants))
                .leftJoin(room.conferences, conference)
                .where(conference.date.notIn(conferenceDate).or(room.conferences.size().eq(0)))
                .limit(10)
                .fetch();
    }
}
