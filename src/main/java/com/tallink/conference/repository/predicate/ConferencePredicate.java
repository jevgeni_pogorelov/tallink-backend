package com.tallink.conference.repository.predicate;

import static com.tallink.conference.model.QConference.conference;
import static com.tallink.conference.model.enums.ConferenceStatus.NEW;
import static com.tallink.conference.model.enums.ConferenceStatus.ROOM_ADDED;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;

import java.time.LocalDate;

public final class ConferencePredicate {

    private ConferencePredicate() {

    }

    public static Predicate searchAvailableConferences(Long participantId) {
        BooleanBuilder condition = new BooleanBuilder();
        condition.and(conference.date.after(LocalDate.now()));
        condition.and(conference.conferenceStatus.eq(NEW).or(conference.conferenceStatus.eq(ROOM_ADDED)));
        condition.andNot(conference.participants.any().id.eq(participantId));

        return condition;
    }
}
