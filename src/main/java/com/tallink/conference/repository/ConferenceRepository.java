package com.tallink.conference.repository;

import com.tallink.conference.model.Conference;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ConferenceRepository extends BaseRepository<Conference>, QuerydslPredicateExecutor<Conference> {

    Optional<Conference> findByName(String name);

}
