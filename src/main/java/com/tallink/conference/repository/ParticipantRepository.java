package com.tallink.conference.repository;

import com.tallink.conference.model.Participant;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface ParticipantRepository extends BaseRepository<Participant> {

    Optional<Participant> findByFullNameAndBirthDate(String fullName, LocalDate birthDate);
}
