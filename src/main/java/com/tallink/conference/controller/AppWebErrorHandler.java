package com.tallink.conference.controller;

import com.tallink.conference.controller.dto.ValidationError;
import com.tallink.conference.controller.dto.WebErrorDetails;
import com.tallink.conference.exception.ConferenceAlreadyExistsException;
import com.tallink.conference.exception.NoAvailableRoomException;
import com.tallink.conference.exception.ParticipantAlreadyExistsException;
import com.tallink.conference.exception.ResourceNotFoundException;
import com.tallink.conference.exception.RoomAlreadyExistsException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;

import java.util.List;
import java.util.Map;

import static com.tallink.conference.AppSpringBeanNames.HIBERNATE_ERROR_MESSAGE_SOURCE_BEAN;
import static java.lang.System.currentTimeMillis;
import static java.util.Collections.emptyMap;
import static org.apache.commons.lang3.StringUtils.defaultIfBlank;

@ControllerAdvice
@Slf4j
public class AppWebErrorHandler {

    private final MessageSource hibernateMessageSource;
    private final LocaleResolver localeResolver;

    @Autowired
    public AppWebErrorHandler(@Qualifier(HIBERNATE_ERROR_MESSAGE_SOURCE_BEAN) MessageSource hibernateMessageSource,
                              LocaleResolver localeResolver) {
        this.hibernateMessageSource = hibernateMessageSource;
        this.localeResolver = localeResolver;
    }

    @ExceptionHandler(RoomAlreadyExistsException.class)
    public ResponseEntity<? extends WebErrorDetails> handleRoomAlreadyExistsException(RoomAlreadyExistsException e,
                                                                                      HttpServletRequest request) {
        final String localizedErrorMessage = hibernateMessageSource.getMessage(e.getMessageKey(), e.getParameters(),
                localeResolver.resolveLocale(request));
        log.error(localizedErrorMessage, e.getHttpStatus());
        return new ResponseEntity<>(generateWebErrorDescription(request, "Room already exists", e.getErrorCode(),
                localizedErrorMessage, emptyMap()), null, e.getHttpStatus());
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<? extends WebErrorDetails> handleResourceNotFoundException(ResourceNotFoundException e,
                                                                                     HttpServletRequest request) {
        final String localizedErrorMessage = hibernateMessageSource.getMessage(e.getMessageKey(), e.getParameters(),
                localeResolver.resolveLocale(request));
        log.error(localizedErrorMessage, e.getHttpStatus());
        return new ResponseEntity<>(generateWebErrorDescription(request, "Resource not found", e.getErrorCode(),
                localizedErrorMessage, emptyMap()), null, e.getHttpStatus());
    }

    @ExceptionHandler(NoAvailableRoomException.class)
    public ResponseEntity<? extends WebErrorDetails> handleNoAvailableRoomException(NoAvailableRoomException e,
                                                                                    HttpServletRequest request) {
        final String localizedErrorMessage = hibernateMessageSource.getMessage(e.getMessageKey(), e.getParameters(),
                localeResolver.resolveLocale(request));
        log.error(localizedErrorMessage, e.getHttpStatus());
        return new ResponseEntity<>(generateWebErrorDescription(request, "No Available Room", e.getErrorCode(),
                localizedErrorMessage, emptyMap()), null, e.getHttpStatus());
    }

    @ExceptionHandler(ConferenceAlreadyExistsException.class)
    public ResponseEntity<? extends WebErrorDetails> handleConferenceAlreadyExistsException(ConferenceAlreadyExistsException e,
                                                                                            HttpServletRequest request) {
        final String localizedErrorMessage = hibernateMessageSource.getMessage(e.getMessageKey(), e.getParameters(),
                localeResolver.resolveLocale(request));
        log.error(localizedErrorMessage, e.getHttpStatus());

        return new ResponseEntity<>(generateWebErrorDescription(request, "Conference already exists", e.getErrorCode(),
                localizedErrorMessage, emptyMap()), null, e.getHttpStatus());
    }

    @ExceptionHandler(ParticipantAlreadyExistsException.class)
    public ResponseEntity<? extends WebErrorDetails> handleParticipantAlreadyExistsException(ParticipantAlreadyExistsException e,
                                                        HttpServletRequest request) {
        final String localizedErrorMessage = hibernateMessageSource.getMessage(e.getMessageKey(), e.getParameters(),
                localeResolver.resolveLocale(request));
        log.error(localizedErrorMessage, e.getHttpStatus());

        return new ResponseEntity<>(generateWebErrorDescription(request, "Participant already exists", e.getErrorCode(),
                localizedErrorMessage, emptyMap()), null, e.getHttpStatus());
    }

    private WebErrorDetails generateWebErrorDescription(HttpServletRequest request, String title, String code,
                                                        String detail, Map<String, List<ValidationError>> errors) {
        return WebErrorDetails.builder()
                .timestamp(currentTimeMillis())
                .title(title)
                .code(code)
                .details(detail)
                .path(pathFrom(request))
                .errors(errors)
                .build();
    }

    private static String pathFrom(final HttpServletRequest request) {
        return defaultIfBlank((String) request.getAttribute("javax.servlet.error.request_uri"), request.getRequestURI());
    }
}
