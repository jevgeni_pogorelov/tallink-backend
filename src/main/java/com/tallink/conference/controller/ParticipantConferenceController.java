package com.tallink.conference.controller;

import com.tallink.conference.model.dto.AddParticipantToConferenceDto;
import com.tallink.conference.service.ParticipantConferenceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static com.tallink.conference.AppConstants.API_URI;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RestController
@RequestMapping(API_URI + "/participantConference")
@RequiredArgsConstructor
@Slf4j
public class ParticipantConferenceController {

    private final ParticipantConferenceService participantConferenceService;

    @PostMapping("/addToConference")
    @ResponseStatus(NO_CONTENT)
    public void addParticipantToConference(@RequestBody AddParticipantToConferenceDto addParticipantToConferenceDto) {
        log.info("POST called on api/participantConference/addToConference resource");
        participantConferenceService.addParticipantToConference(addParticipantToConferenceDto);
    }

    @DeleteMapping("/removeFromConference")
    @ResponseStatus(NO_CONTENT)
    public void removeParticipantFromConference(@RequestParam("participantId") Long participantId, @RequestParam("conferenceId") Long conferenceId) {
        log.info("POST called on api/participantConference/removeFromConference resource");
        participantConferenceService.removeParticipantFromConference(participantId, conferenceId);
    }
}
