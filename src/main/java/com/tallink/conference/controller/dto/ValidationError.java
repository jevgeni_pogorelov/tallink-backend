package com.tallink.conference.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ValidationError implements Serializable {

    private String code;
    private String message;

    public static ValidationError of(String code, String message) {
        return new ValidationError(code, message);
    }
}
