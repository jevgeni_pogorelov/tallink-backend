package com.tallink.conference.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WebErrorDetails {

    private String title;
    private String path;
    private String details;
    private String code;
    private Long timestamp;

    @Builder.Default
    private Map<String, List<ValidationError>> errors = new HashMap<>();
}
