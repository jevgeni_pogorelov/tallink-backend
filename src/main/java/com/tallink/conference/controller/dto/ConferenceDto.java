package com.tallink.conference.controller.dto;

import com.tallink.conference.model.enums.ConferenceStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ConferenceDto {

    private Long id;
    private String name;
    private LocalDate date;
    private LocalTime time;
    private RoomDto room;
    private ConferenceStatus conferenceStatus;
    private int numberOfParticipants;

}
