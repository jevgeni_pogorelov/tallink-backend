package com.tallink.conference.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ParticipantDetailDto {

    private Long id;
    private String fullName;
    private LocalDate birthDate;
    private List<ConferenceDto> conferences;
}
