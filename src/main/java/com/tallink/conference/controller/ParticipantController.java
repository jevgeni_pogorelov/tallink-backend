package com.tallink.conference.controller;

import com.tallink.conference.controller.dto.ParticipantDetailDto;
import com.tallink.conference.controller.dto.ParticipantDto;
import com.tallink.conference.model.dto.CreateParticipantDto;
import com.tallink.conference.service.ParticipantService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.tallink.conference.AppConstants.API_URI;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;


@RestController
@RequestMapping(API_URI + "/participant")
@RequiredArgsConstructor
@Slf4j
public class ParticipantController {

    private final ParticipantService participantService;

    @PostMapping
    @ResponseStatus(NO_CONTENT)
    public void createParticipant(@RequestBody CreateParticipantDto createParticipantDto) {
        log.info("POST called on api/participant resource");
        participantService.createParticipant(createParticipantDto);
    }

    @GetMapping
    public ResponseEntity<Page<ParticipantDto>> allParticipants(@PageableDefault Pageable pageable) {
        log.info("GET called on api/participant resource");
        return new ResponseEntity<>(participantService.allParticipants(pageable), OK);
    }

    @GetMapping("/participantConferences/{participantId}")
    public ResponseEntity<ParticipantDetailDto> getAllParticipantConferences(@PathVariable Long participantId) {
        log.info("GET called on api/participant/" + participantId + " resource");
        return new ResponseEntity<>(participantService.getAllParticipantConferences(participantId), OK);

    }

}
