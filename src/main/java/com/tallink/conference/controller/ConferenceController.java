package com.tallink.conference.controller;

import com.tallink.conference.controller.dto.ConferenceDto;
import com.tallink.conference.model.dto.AddRoomToConferenceDto;
import com.tallink.conference.model.dto.CreateConferenceDto;
import com.tallink.conference.model.dto.UpdateConferenceDto;
import com.tallink.conference.service.ConferenceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static com.tallink.conference.AppConstants.API_URI;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(API_URI + "/conference")
@RequiredArgsConstructor
@Slf4j
public class ConferenceController {

    private final ConferenceService conferenceService;

    @PostMapping
    @ResponseStatus(NO_CONTENT)
    public void createConference(@RequestBody CreateConferenceDto createConferenceDto) {
        log.info("POST called on api/conference resource");
        conferenceService.createConference(createConferenceDto);
    }

    @PostMapping("/update")
    @ResponseStatus(NO_CONTENT)
    public void updateConference(@RequestBody UpdateConferenceDto updateConferenceDto) {
        log.info("POST called on api/conference/update resource");
        conferenceService.updateConference(updateConferenceDto);
    }

    @GetMapping
    public ResponseEntity<Page<ConferenceDto>> allConferences(@PageableDefault Pageable pageable) {
        log.info("GET called on api/conference resource");
        return new ResponseEntity<>(conferenceService.allConferences(pageable), OK);
    }

    @PostMapping("/cancel/{conferenceId}")
    @ResponseStatus(NO_CONTENT)
    public void cancelConference(@PathVariable Long conferenceId) {
        log.info("POST called on api/conference/cancel/" + conferenceId + "resource");
        conferenceService.cancelConference(conferenceId);
    }

    @PostMapping("/addRoom")
    @ResponseStatus(NO_CONTENT)
    public void addRoomToConference(@RequestBody AddRoomToConferenceDto addRoomToConferenceDto) {
        log.info("POST called on api/conference/addRoom resource");
        conferenceService.addRoomToConference(addRoomToConferenceDto);
    }

    @GetMapping("/availableToParticipate")
    public ResponseEntity<Page<ConferenceDto>> findAvailableConferencesToParticipate(@PageableDefault Pageable pageable, @RequestParam(value = "participantId") Long participantId) {
        log.info("GET called on api/conference/availableToParticipate resource");
        return new ResponseEntity<>(conferenceService.findAvailableConferencesToParticipate(pageable, participantId), OK);
    }
}
