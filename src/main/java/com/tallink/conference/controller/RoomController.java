package com.tallink.conference.controller;

import com.tallink.conference.controller.dto.RoomDto;
import com.tallink.conference.model.dto.CreateRoomDto;
import com.tallink.conference.model.dto.UpdateRoomDto;
import com.tallink.conference.service.RoomService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.tallink.conference.AppConstants.API_URI;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(API_URI + "/room")
@RequiredArgsConstructor
@Slf4j
public class RoomController {

    private final RoomService roomService;

    @PostMapping
    @ResponseStatus(NO_CONTENT)
    public void createRoom(@RequestBody CreateRoomDto createRoomDto) {
        log.info("POST called on api/room resource");
        roomService.createRoom(createRoomDto);
    }

    @PostMapping("/update")
    @ResponseStatus(NO_CONTENT)
    public void updateRoom(@RequestBody UpdateRoomDto updateRoomDto) {
        log.info("POST called on api/room/update resource");
        roomService.updateRoom(updateRoomDto);
    }

    @GetMapping
    public ResponseEntity<Page<RoomDto>> allRooms(@PageableDefault Pageable pageable) {
        log.info("GET called on api/room resource");
        return new ResponseEntity<>(roomService.findAllRooms(pageable), OK);
    }

    @GetMapping("/{conferenceId}")
    public ResponseEntity<List<RoomDto>> findAvailableRoomByConferenceId(@PathVariable Long conferenceId) {
        log.info("GET called on api/room/" + conferenceId + " resource");
        return new ResponseEntity<>(roomService.findAvailableRoomByConferenceId(conferenceId), OK);
    }
}
