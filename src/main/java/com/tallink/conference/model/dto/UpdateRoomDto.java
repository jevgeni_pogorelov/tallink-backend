package com.tallink.conference.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UpdateRoomDto {

    @NotNull
    private Long id;
    @NotBlank
    private String name;
    @NotBlank
    private String Location;
    @NotNull
    private int maxSeats;
}
