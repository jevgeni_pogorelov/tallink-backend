package com.tallink.conference.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AddParticipantToConferenceDto {

    @NotNull
    private Long participantId;
    @NotNull
    private Long conferenceId;
}
