package com.tallink.conference.model.enums;

import org.apache.commons.lang3.StringUtils;

public interface AppEnum extends WithCode {

    String getCode();

    default boolean equals(String code) {
        return StringUtils.equals(getCode(), code);
    }
}
