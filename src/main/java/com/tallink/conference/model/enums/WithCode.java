package com.tallink.conference.model.enums;

public interface WithCode {

    String getCode();
}
