package com.tallink.conference.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum ConferenceStatus implements AppEnum{

    NEW("NEW"),
    ROOM_ADDED("ROOM_ADDED"),
    CANCELED("CANCELED");

    @Getter
    private final String code;
}
