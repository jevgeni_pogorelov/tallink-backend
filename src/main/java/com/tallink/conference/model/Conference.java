package com.tallink.conference.model;

import com.tallink.conference.model.enums.ConferenceStatus;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.FetchType.LAZY;

@Data
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true, exclude = "participants")
@Entity
@Table(name = "CONFERENCE")
public class Conference extends AbstractEntity {

    @Size(max = 150)
    @NotBlank
    @Column(name = "NAME", length = 150, unique = true, nullable = false)
    private String name;

    @NotNull
    @Column(name = "DATE", nullable = false)
    private LocalDate date;

    @NotNull
    @Column(name = "TIME", nullable = false)
    private LocalTime time;

    @ManyToOne(fetch = LAZY)
    @JoinColumn(name = "ROOM_ID")
    private Room room;

    @Enumerated(STRING)
    @NotNull
    @Column(name = "CONFERENCE_STATUS", length = 15, nullable = false)
    private ConferenceStatus conferenceStatus;

    @ManyToMany(fetch = LAZY)
    @JoinTable(name = "PARTICIPANT_CONFERENCE",
            joinColumns = @JoinColumn(name = "CONFERENCE_ID", referencedColumnName = "ID", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "PARTICIPANT_ID", referencedColumnName = "ID", nullable = false)
    )
    private Set<Participant> participants = new HashSet<>();

    @Builder
    public Conference(Long id,
                      LocalDateTime created,
                      LocalDateTime modified,
                      @NotBlank @Size(max = 150) String name,
                      @NotNull LocalDate date,
                      @NotNull LocalTime time,
                      Room room,
                      @NotNull ConferenceStatus conferenceStatus,
                      Set<Participant> participants) {
        super(id, created, modified);
        this.name = name;
        this.date = date;
        this.time = time;
        this.room = room;
        this.conferenceStatus = conferenceStatus;
        this.participants = participants;
    }
}
