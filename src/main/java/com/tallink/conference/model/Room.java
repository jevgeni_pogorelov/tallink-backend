package com.tallink.conference.model;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Data
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "ROOM")
public class Room extends AbstractEntity {

    @Size(max = 150)
    @NotBlank
    @Column(name = "NAME", unique = true, length = 150, nullable = false)
    private String name;

    @Size(max = 150)
    @NotBlank
    @Column(name = "LOCATION", length = 150, nullable = false)
    private String location;

    @NotNull
    @Column(name = "MAX_SEATS", nullable = false)
    private int maxSeats;

    @OneToMany(mappedBy = "room")
    private List<Conference> conferences = new ArrayList<>();

    @Builder
    public Room(Long id,
                LocalDateTime created,
                LocalDateTime modified,
                String name,
                String location,
                int maxSeats,
                List<Conference> conferences) {
        super(id, created, modified);
        this.name = name;
        this.location = location;
        this.maxSeats = maxSeats;
        this.conferences = conferences;
    }
}
