package com.tallink.conference.model;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import java.time.LocalDateTime;

import static javax.persistence.FetchType.LAZY;

@Data
@NoArgsConstructor
@ToString(callSuper = true, exclude = {"conference", "participant"})
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "PARTICIPANT_CONFERENCE")
public class ParticipantConference extends AbstractEntity {

    @ManyToOne(fetch = LAZY)
    @NotNull
    @JoinColumn(name = "CONFERENCE_ID")
    private Conference conference;

    @ManyToOne(fetch = LAZY)
    @NotNull
    @JoinColumn(name = "PARTICIPANT_ID")
    private Participant participant;

    @Builder
    public ParticipantConference(Long id,
                       LocalDateTime created,
                       LocalDateTime modified,
                       @NotNull Conference conference,
                       @NotNull Participant participant) {
        super(id, created, modified);
        this.conference = conference;
        this.participant = participant;
    }
}
