package com.tallink.conference.model;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "PARTICIPANT")
public class Participant extends AbstractEntity {

    @Size(max = 30)
    @NotBlank
    @Column(name = "FULL_NAME", unique = true, length = 30, nullable = false)
    private String fullName;

    @NotNull
    @Column(name = "BIRTH_DATE", nullable = false)
    private LocalDate birthDate;

    @ManyToMany(mappedBy = "participants")
    private Set<Conference> conferences = new HashSet<>();

    @Builder
    public Participant(Long id,
                       LocalDateTime created,
                       LocalDateTime modified,
                       @NotBlank String fullName,
                       @NotNull LocalDate birthDate,
                       Set<Conference> conferences) {
        super(id, created, modified);
        this.fullName = fullName;
        this.birthDate = birthDate;
        this.conferences = conferences;
    }
}
