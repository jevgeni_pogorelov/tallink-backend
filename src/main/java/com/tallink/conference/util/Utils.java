package com.tallink.conference.util;

import java.util.Collection;
import java.util.stream.Stream;

public final class Utils {

    private Utils() {

    }

    public static <O> Stream<O> nullSafeStream(Collection<O> collectionOrNull) {
        if (collectionOrNull == null) {
            return Stream.empty();
        }
        return collectionOrNull.stream();
    }
}
